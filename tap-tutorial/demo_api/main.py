from fastapi import FastAPI
import requests

app = FastAPI()


@app.get("/{page}")
async def root(page):
    if not page:
        page = 1
    response = requests.get('https://rickandmortyapi.com/api/character/?page='+ page)
    return response.json()